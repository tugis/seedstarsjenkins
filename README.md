# Seedstars Jenkins Challenge

This project is a command line app that retrieves the list of jobs running for a given [Jenkins](https://jenkins-ci.org) instance and stores the results on sqlite database (optional parameter to set the sqlite database name).

It was done using [Cilex](https://github.com/Cilex/Cilex), a simple command line application framework based on Symfony2 components.

## Installation

- Update PHP modules dependencies
```
composer update
```
## Usage

Run:
```
php run.php get:jobs <jenkins_url> <sqlite_bd_name>
```

Example:
```
php run.php get:jobs http://localhost:8080 jobs.sqlite
```